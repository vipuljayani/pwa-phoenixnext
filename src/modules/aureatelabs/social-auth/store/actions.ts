import Vue from 'vue'
import { ActionTree } from 'vuex'
import * as types from './mutation-types'
import RootState from '@vue-storefront/core/types/RootState'
import AuthState from '../types/AuthState'

const actions: ActionTree<AuthState, RootState> = {
  setLoginUser ({ commit }, user) {
    commit(types.SET_LOGIN_USER, user)
    if (typeof user !== 'string') {
      user = JSON.stringify(user)
    }
    return localStorage.setItem('user', user)
  }

}

export default actions
